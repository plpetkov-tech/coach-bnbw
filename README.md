# Coach BNBW (be nice, be well)

Coach BNBW is a python program that analyses the behaviour of the contributors of a specified git repository, **powered by AI.**

The Coach's purpose is to work as a guard rail of sorts for developers. It gathers information through the .git file and 
performs simple mathematical analysis in order to gather information about the behavior of the contributors.
Once the information is gathered, it's passed off to an AI to give an opinion and maybe even a suggestion how to avoid
overworking and encourage mental wellbeing. 

# What does Coach BNBW analyse exactly?

### Currently, it analyses the following:

- **Sentiment analysis of commit messages**, *powered by AI.* 
Rates the messages as having a *negative, positive or neutral* sentiment.

>NOTE: There is an edge case called *neutrative*, which is a AI halucination that has popped up a few times in testing.
>Always happened when the expected result was negative, so if the AI responds with *neutrative*, it's considered as negative.

Sentiment analysis can, in a way, track the emotional status of the contributor, at the time of the commit. It can indicate frustration, hopelessness, happiness, hopefulness or just plain normal, informative messages.

- **Repetitive commit messages.**
Coach makes note if your commit messages are similar, which might indicate frustration, tiredness or laziness.

- **Stress calculation.**
Stress calculation is a mix of a few factors. 
    - What time the has the commit been made? Was it within or outside working hours?
    - How frequent are the commits?
    - What is the velocity of the commits - how often and how fast are they commited?

The results from these factors is gathered and together they produce a total stress score.

The information is gathered and sent over to an Ollama instance, which analyzes the received information and gives back feedback / advice on what to do based on the results in order to be mentally healthy and avoid burnout.

# How to include Coach BNBW in your pipeline?

Coach BNBW, currently, is designed to work as a part of your pipeline. You can include it into your pipeline by creating a stage where the container is ran. 

In order to set up Coach BNBW, you will need to prepare a configuration file like this (Assuming you want those pieces of configuration replaced in the pipeline during execution, you can secure the key in CI/CD Variables and mask it for extra security): 
```toml
[git]
repo = "${REPOSITORY_PATH_IN_CONTAINER}"
commits = ${REQUESTED_COMMITS}

[ollama]
url = "${OLLAMA_URL}"
key = "${OLLAMA_API_KEY}"
```

And you'll need to include it as a stage in your pipeline. We mount the repo in /work as the Coach love that directory.

#### Example: 

```yaml
coach_stage:
  allow_failure: true
  image: docker:cli
  stage: coach_stage
  services:
    - docker:dind
  script:
    - sed -i 's|${REPOSITORY_PATH_IN_CONTAINER}|'"$CI_BUILDS_DIR"'|' ./coach.toml
    - sed -i 's|${REQUESTED_COMMITS}|'"$REQUESTED_COMMITS"'|' ./coach.toml
    - sed -i 's|${OLLAMA_URL}|'"$OLLAMA_URL"'|' ./coach.toml
    - sed -i 's|${OLLAMA_API_KEY}|'"$OLLAMA_API_KEY"'|' ./coach.toml
    - docker run -v .:/work registry.gitlab.com/coach-bnbw1/coach-bnbw:latest --config /work/coach.toml
```

Create the following CI/CD variables within the pipeline:

    - REQUESTED_COMMITS: The number of commits that you want to use as a dataset (use at least 2 in order to get the best experience)
    - OLLAMA_URL: This is the URL that has been provided for the demo
    - OLLAMA_API_KEY: The API key provided for the demo. Without it you will not be able to access the Ollama API and Coach BNBW will not work

# Where to get the container from? 

### You can [download Coach BNBW on dockerhub](https://hub.docker.com/r/plpetkov/coach_bnbw). 

# Code explanation

### main.py

    Utilizes the modules and serves as the main function of the program.     

    Functionality:
    - Parses command-line arguments to customize the execution of the script.
    - Configures the parser with options for enabling verbose mode, IF FLAG IS PASSED,
      and specifies, IF FLAG IS PASSED, a custom configuration file.
    - Loads the configuration based on the provided or default path.
    - Prints the generated mental wellbeing advice.

### parser.py

    Parse configuration from the specified config file.

    Functionality:

    - Reads the TOML configuration file, extracts relevant information
    about the Git repository, Ollama service, and requested commits.
    - Resolves the Git repository path, retrieves commits, and returns
    a dictionary containing configuration details.

### analyst.py

    Analyzes Git commits, performs sentiment analysis using the Ollama service, and assesses repetitiveness and stress indicators based on commit timestamps.

    Dependencies:
    - datasketch:            Used for MinHash and MinHashLSH computation.
    - functools.lru_cache:   Used for caching results of the 'analyze_sentiment' function.
    - datetime:              Used for handling timestamps and time-related calculations.
    - typing:                Used for type hints.
    - ollama:                Used for interacting with the Ollama service.
    - json:                  Used for handling JSON data.
    - backoff:               Used for implementing backoff strategy during Ollama API calls.
    - os:                    Used for handling environmental variables and file operations.

    Classes:
    - CustomClient:          A customized Ollama client with optional authentication.

    Functions:
    - parse_config:          Parses configuration from a specified file, extracting details about the Git repository, Ollama service, and requested commits.
    - get_commits_by_user:   Retrieves a specified number of commits by a specific user from a Git repository.
    - get_last_commit_author: Gets the name of the author of the last commit in a Git repository.
    - prompt_gen:            Generates a prompt for sentiment analysis.
    - create_ollama_client:  Creates a custom Ollama client with optional authentication.
    - backoff_hdlr:          Handles backoff events during Ollama API calls.
    - analyze_sentiment:     Analyzes the sentiment of a sentence using the Ollama service.
    - tokenize:              Tokenizes a sentence into words.
    - minhash_signature:     Computes the MinHash signature of a sentence.
    - jaccard_similarity:    Computes Jaccard similarity between two sentences.
    - assess_commits_for_repetitiveness: Assesses the repetitiveness of a list of sentences using MinHash LSH.
    - calculate_stress:      Calculates stress indicators and score based on commit timestamps.

    Note:
    - The script uses external libraries such as datasketch, ollama, backoff, etc.
    - Sentiment analysis is performed using the Ollama service.
    - The stress calculation is based on commit timestamps and various indicators.

### decision_manager.py

    Performs a comprehensive analysis of Git commits, provides decision-based insights, and generates advice using sentiment analysis and Ollama service paraphrasing.

    Dependencies:
    - time:                 Used for timestamp-related operations.
    - backoff:              Used for implementing backoff strategy during Ollama API calls.
    - ollama:               Used for interacting with the Ollama service.
    - functools.lru_cache:  Used for caching results of the 'ai_advice' function.
    - modules.analyst:      Module containing helper functions for the decision workflow.

    Functions:
    - prompt_gen_output:    Generates a prompt for Ollama paraphrasing based on an input message.
    - ai_advice:            Generates advice for the user based on the paraphrased input using the Ollama service.
    - decisions_workflow:   Performs a comprehensive analysis of commits and provides decision-based insights.

    Note:
    - This script relies on external libraries and Ollama service for sentiment analysis and paraphrasing.
    - The 'decisions_workflow' function analyzes commit frequency, commit timestamps, velocity, repetitiveness, and sentiment.
    - The 'ai_advice' function generates advice for the user based on the analysis.

### output.py

    Provides a function for formatting and presenting a message in a dog-themed box using the 'boxes' command-line tool.

    Dependencies:
    - subprocess: Used for running shell commands.

    Function:
    - dog_output: Takes a message as input, formats it in a dog-themed box using the 'boxes' command, and returns the formatted output.


# TODO:
- [X] Config parser should take decisions based on where the container is run from     
    - [X] If is run locally or standalone as container, looks at the config for an url or path(?)    
    - [X] If is run from a pipeline would be known as you would have the repo directly in /work mounted     
- [X] Fix the comparassion logic   
- [X] Create tests before features   
- [X] Create mocks for tests   
- [X] Create beautiful documentation *After finishing the project   
- [ ] Create logic to push the image to gitlab registry after passing the pipeline  
    + it shoould be tagged with the short sha and whenever we give some flag to the commit like `__push_to_prod` it can version the tag in a nifty way   
- [X] Add a function that calculates the velocity of the commits (number of commits for a time period)
    - [X] Added, now test it and fix it
    - [X] Create unit tests that involve the velocity function
- [ ] Investigate a way to use `include` in gitlab-ci.yml https://docs.gitlab.com/ee/ci/yaml/includes.html -> people can include our config, which just contains the stage for the coach container and copies the repository into the /work/ volume
- [ ] Fix `error_handler.py` - it needs to except errors that are from GitPython library
- [X] Add functionality so that the program can distinguish between different authors / committers
    - [X] Get the authors and put them in the dict
    - [X] Distinguish between different actors and make a nested dict, where only the commits of the author are stored
    - [ ] Create unit tests that cover the new functionality
- [X] Update `decision_manager.py`
    - [X] Make the function in `decision_manager.py` take a list of the sentiments for the sentences and create mathematical functions that give a result based on the total sentiment & repetitiveness of the commit messages of the author
- [ ] Maybe add branch functionality? 
    - [ ] Scan author's commits across all branches?
 
