import time,backoff,ollama
from functools import lru_cache
from modules.analyst import create_ollama_client, CustomClient,backoff_hdlr
from modules.analyst import analyze_sentiment 
from modules.analyst import assess_commits_for_repetitiveness
from modules.analyst import calculate_stress 

def prompt_gen_output(msg):
    return f"""You are an expert consultant in psychology, experienced with helping programmers. Paraphrase the following phrases in a joint harmonious and wise phrase (have in mind this advices come from empirical data that you have), also is really important that you give ONLY and ONLY the result full phrase, no add ins, introductions or whatsoever ONLY the full result phrase. :\n {msg}"""

@backoff.on_exception(backoff.expo,
                        ValueError,
                      max_tries=2,
                      on_backoff=backoff_hdlr)
@lru_cache(maxsize=30)  # Cache up to 30 resultss

def ai_advice(sentence: str, client: CustomClient) -> str:

    """
    Args:
    sentence (str): The input string for paraphrasing as an expert.
    client (CustomClient): An instance of the CustomClient class.

    Returns:
    str: The advice that is going to benefit our users.
    """
        
    result = ''
    try:
        response = client.generate(
            model='mistral:latest', prompt=f'{prompt_gen_output(sentence)}')
        result = response['response']
    except:
        raise ValueError
    return result


def decisions_workflow(conf_obj: dict) -> str:
    """
    Perform a comprehensive analysis of commits and provide decision-based insights.

    Args:
    conf_obj (dict): Configuration object containing commit details.

    Returns:
    dict: A dictionary containing decisions and insights based on the analysis.
    """

    clean_msgs = []
    commit_timestamps = []
    for commit in conf_obj["commits"]: 
        clean_timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(commit.committed_date))
        msg = str.strip(commit.message)
        commit_timestamps.append(clean_timestamp)
        clean_msgs.append(msg)

    # the return value from this func, holds decisions concluded based on the analysis
    return_dict = {}

    # return values for the decisions
    decisions_dict = {
        'frequent_commits': 'You are committing too frequently. You are probably going to upload code that needs more attention. Take a break first and revisit the problem later.',
        'regular_commits': 'You are not committing too frequently, which is good. I am sure your code will work properly!',
        'irregular_time_commits': 'You seem to be committing during non-standard work hours. Relax, you can continue working tomorrow during regular hours!',
        'regular_time_commits': 'You seem to be committing within standard work hours. That indicates a good work-life balance!',
        'high_velocity_commits': 'You are committing too fast! Consider slowing down and reviewing your changes.',
        'normal_velocity_commits': 'Your commits are well spaced out. Good job maintaining a steady pace!',
        'stressed_commits': 'You seem to be stressed. Take a break before continuing to ensure a healthy work environment.',
        'repetitive_commits': 'Consider providing more descriptive commit messages for better code understanding.',
        'not_repetitive_commits': 'Your commit messages are descriptive! Good job communicating your changes.',
        'negative_commits': 'Your commit messages convey a negative sentiment. Reach out for support if needed.',
        'positive_commits': 'You seem happy and satisfied! Keep up the positive momentum!',
        'neutral_commits': 'You seem to be having a normal workday. If any challenges arise, feel free to seek assistance.'
    }
    
    host = conf_obj['ollama_url']
    password = conf_obj['api_key']
    client = create_ollama_client(host,password)
    sentiment_score = 0
    # SENTIMENT ANALYSIS
    for message in clean_msgs:    
        sentiment_result = analyze_sentiment(message,client)
        # Giving a score for each type of sentiment
        match sentiment_result:
            case "Neutral":
                sentiment_score =+ 0.01
            
            case "Positive":
                sentiment_score =+ 1.5
        
            case "Negative":
                sentiment_score =- 1.6
            
            # ai halucination edge case LOL, during testing it happened only for statements
            # that was set as an expectation for negative, so we're going to use it as an
            # edge case for negative
            case "Neutrative":
                sentiment_score =- 0.8
    # Analyzing score to give overall result of sentiment
    sentiment_score = sentiment_score / len(clean_msgs)
    match sentiment_score:
        case _ if sentiment_score > 0 and sentiment_score < 0.06:
            return_dict['sentiment'] = decisions_dict['neutral_commits']
        
        case _ if sentiment_score > 0.06:
            return_dict['sentiment'] = decisions_dict['positive_commits']

        case _ if sentiment_score < 0:
            return_dict['sentiment'] = decisions_dict['negative_commits']

    # repetitiveness decision
    repetitiveness_assesment = assess_commits_for_repetitiveness(clean_msgs)
    
    if float(repetitiveness_assesment.split("%")[0]) < 30.0:
        return_dict['repetitiveness'] = decisions_dict['not_repetitive_commits']
    else:
        return_dict['repetitiveness'] = decisions_dict['repetitive_commits']

    # stress decision
    stress_assesment = calculate_stress(commit_timestamps)

    # frequency
    if stress_assesment['Stress Indicators']['High Commit Frequency'] == True:
        return_dict['commit_frequency'] = decisions_dict['frequent_commits']
    else:
        return_dict['commit_frequency'] = decisions_dict['regular_commits']

    # based on time
    if stress_assesment['Stress Indicators']['Irregular Commit Times'] == True:
        return_dict['commit_times'] = decisions_dict['irregular_time_commits']
    else:
        return_dict['commit_times'] = decisions_dict['regular_time_commits']
    
    # velocity
    if stress_assesment['Stress Indicators']['High Velocity'] == True:
        return_dict['velocity'] = decisions_dict['high_velocity_commits']
    else:
        return_dict['velocity'] = decisions_dict['normal_velocity_commits']

    combined_string = ''

    for value in return_dict.values():
        if isinstance(value, str):
            combined_string = combined_string + "\n" + value
    try:
        return_advice = ai_advice(combined_string,client)
    except:
        return_advice = combined_string

    return return_advice