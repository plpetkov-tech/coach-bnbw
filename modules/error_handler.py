# Exceptions for problems with the repo flag
class InvalidGitRepositoryError (Exception):
    "Raised when the repo cannot be found or is wrongly configured"
    def __init__(self, message="Repo provided is not valid"):
        self.message = message
        super().__init__(self.message)

# Exceptions for problems with the config parameters
class InvalidParametersException(Exception):
    "Raised when the config has invalid values or something went wrong parsing the config"
    def __init__(self, message="One or more parameters in the config provided are not valid"):
        self.message = message
        super().__init__(self.message)
# Runtime exceptions
class CoachRuntimeException(Exception):
    "Raised when something goes wrong in the runtime"
    def __init__(self, message="CoachRuntime: Something happened while running the program"):
        self.message = message
        super().__init__(self.message)