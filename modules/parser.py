import os, toml, git
from modules.error_handler import *

def parse_config(config_path: str) -> dict:

    """
    Parse configuration from the specified file.

    Reads the TOML configuration file, extracts relevant information
    about the Git repository, Ollama service, and requested commits.
    Resolves the Git repository path, retrieves commits, and returns
    a dictionary containing configuration details.

    Args:
    config_path (str): The path to the configuration file.

    Returns:
    dict: A dictionary with the following keys:
        - "ollama_url": The URL of the Ollama service.
        - "api_key": The API key for accessing the Ollama service.
        - "repo": A Git repository object.
        - "commits": A list of Git commits.
        - "requested_number_of_commits": The requested number of commits.
    """
    
    with open(config_path, 'r') as file:
        config = toml.load(file)

    repo_path = config.get('git', {}).get('repo')
    requested_number_of_commits = config.get('git', {}).get('commits')
    ollama_url = config.get('ollama', {}).get('url')
    key = config.get('ollama', {}).get('key')

    ## THIS WAS CREATED FOR TESTING PURPOSES, SHOULD WE REMOVE IT?
    if os.path.isdir(('/work/.git')):
        repo = git.Repo('/work')
    else:
        try:
            repo = git.Repo(repo_path)
        except InvalidGitRepositoryError:
            print(f'Specified git repo path {repo_path} was not found!')
            exit(1)

    try:
        commits = get_commits_by_user(
            repo, get_last_commit_author(repo), requested_number_of_commits)
    except:
        print('Error trying to get commits')
        commits = []

    return {
        "ollama_url": ollama_url,
        "api_key": key,
        "repo": repo,
        "commits": commits,
        "requested_number_of_commits": requested_number_of_commits
    }


def get_commits_by_user(repo, username, commitConf):

    """
    Retrieve a specified number of commits by a specific user from a Git repository.

    Args:
    repo: A Git repository object.
    username (str): The username of the desired commit author.
    commitConf (int): The requested number of commits.

    Returns:
    list: A list of Git commits authored by the specified user.
    """

    commits = list(repo.iter_commits())
    commits_by_user = [
        commit for commit in commits if commit.author.name == username]
    if (len(commits_by_user) < commitConf):
        print(
            'Requested commits are more than actual commits, giving max amount of commits')
        return commits_by_user
    return commits_by_user[:commitConf]


def get_last_commit_author(repo):

    """
    Get the name of the author of the last commit in a Git repository.

    Args:
    repo: A Git repository object.

    Returns:
    str: The name of the author of the last commit.
    """

    last_commit = repo.head.commit
    author_name = last_commit.author.name
    return author_name
