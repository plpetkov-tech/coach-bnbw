from datasketch import MinHash, MinHashLSH
from functools import lru_cache
from datetime import datetime, timedelta
from typing import Optional
from ollama import Client as OriginalClient
import json, ollama, backoff, os


class CustomClient(OriginalClient):
    def __init__(self, host: Optional[str] = None, password: Optional[str] = None, **kwargs) -> None:

        """

        Customized Ollama Client with optional authentication.

        Args:
        host (Optional[str]): The host URL of the Ollama service.
        password (Optional[str]): The password for authentication.
        **kwargs: Additional keyword arguments passed to the parent class constructor.

        """

        # Call the constructor of the parent class
        super().__init__(host, **kwargs)

        # Construct the Authorization header
        if password:
            auth_token = f"Bearer coach:{password}"
            self._client.headers['Authorization'] = auth_token


def prompt_gen(s: str) -> str:

    """
    Generate a prompt for sentiment analysis.

    Args:
    s (str): The input sentence for sentiment analysis.

    Returns:
    str: A formatted prompt for the Ollama service.
    """

    return """
Give me a sentiment analysis of the following sentence, ONLY output the result with options: "Negative", "Positive" or "Neutral", nothing else no explanations, no comments, nothing more, return it in a md formatted json as a list of objects like  {"result":result}
Have in mind that this is a commit message in git, so whenever you have cursed words count it as a negative, otherwise consider mostly the level of respectfulness, dont mistake for negative things that are neutral, your threshold of negative should be high except for when curse words are used.
---
"""+s

def create_ollama_client(host: str, password: str) ->  Optional[CustomClient]:

    """
    Create a custom Ollama client with optional authentication.

    Args:
    host (str): The host URL of the Ollama service.
    password (str): The password for authentication.

    Returns:
    CustomClient: An instance of the CustomClient class.
    """

    if os.environ.get('default_config_coach'):
        try:
            c = OriginalClient(host)
            return c
        except:
            print('No available Ollama api')
            exit(1)
    client = CustomClient(host, password)
    return client

def backoff_hdlr(details: dict) -> None:

    """
    Handle backoff events during Ollama API calls.

    Args:
    details (dict): Details of the backoff event.
    """

    print ("Backing off {wait:0.1f} seconds after {tries} tries "
           "calling function {target} with args {args} and kwargs "
           "{kwargs}".format(**details))

@backoff.on_exception(backoff.expo,
                      ollama.ResponseError,
                      max_tries=2,
                      on_backoff=backoff_hdlr)
@lru_cache(maxsize=30)  # Cache up to 30 results

def analyze_sentiment(sentence: str, client: CustomClient) -> str:

    """
    Analyze the sentiment of a sentence using the Ollama service.

    Args:
    sentence (str): The input sentence for sentiment analysis.
    client (CustomClient): An instance of the CustomClient class.

    Returns:
    str: The sentiment result ("Negative", "Positive", or "Neutral").
    """
        
    result = ''
    try:
        response = client.generate(
                model='mistral:latest', prompt=f'{prompt_gen(sentence)}')
        # print(f'DEBUG: {response}')
        result = json.loads(str(response).split("'response': ' ")[1].split(
                "', 'done':")[0].split("\\n")[0])[0]['result'].strip()
    except:
        return result
    return result

def tokenize(sentence: str) -> set:

    """
    Tokenize a sentence into words.

    Args:
    sentence (str): The input sentence.

    Returns:
    set: A set of words obtained by tokenizing the sentence.
    """

    return set(sentence.split())

def minhash_signature(sentence: str, num_perm: int = 128) -> MinHash:

    """
    Compute the MinHash signature of a sentence.

    Args:
    sentence (str): The input sentence.
    num_perm (int): The number of permutations for MinHash computation.

    Returns:
    MinHash: The MinHash signature of the sentence.
    """

    m = MinHash(num_perm=num_perm)
    for word in tokenize(sentence):
        m.update(word.encode('utf-8'))
    return m

def jaccard_similarity(sentence1: str, sentence2: str) -> float:

    """
    Compute Jaccard similarity between two sentences.

    Args:
    sentence1 (str): The first input sentence.
    sentence2 (str): The second input sentence.

    Returns:
    float: The Jaccard similarity between the sentences.
    """

    set1 = tokenize(sentence1)
    set2 = tokenize(sentence2)
    intersection_size = len(set1.intersection(set2))
    union_size = len(set1.union(set2))
    return intersection_size / union_size


def assess_commits_for_repetitiveness(sentences: list) -> str:
    """
    Assess the repetitiveness of a list of sentences using MinHash LSH.

    Args:
    sentences (list): A list of sentences.

    Returns:
    str: The average repetitiveness percentage.
    """

    minhashes = [minhash_signature(sentence) for sentence in sentences]

    lsh = MinHashLSH(threshold=0.5, num_perm=128)

    for i, minhash in enumerate(minhashes):
        lsh.insert(str(i), minhash)

    duplicates = set()
    for i, sentence in enumerate(sentences):
        result = lsh.query(minhash_signature(sentence))
        result.remove(str(i))  # Remove the sentence itself from the results
        duplicates.update(result)

    scores = []
    memo = {}
    # use for in range to track index
    for i in range(len(sentences)):
        for j in range(len(sentences)):
            combination1 = (i, j) #TUPLE
            combination2 = (j, i)

            if combination1 in memo or combination2 in memo:
                continue

            else:
                similarity = jaccard_similarity(sentences[i], sentences[j])
                scores.append(similarity)
                memo[combination1] = True
                memo[combination2] = True

    if len(scores) == 0:
        return "No similarity scores available. Cannot compute average repetitiveness."

    average_repetitiveness = round(sum(scores) / len(scores), 2) * 100
    return str(average_repetitiveness) + "%"



def calculate_stress(commit_timestamps: list) -> Optional[dict]:
    """
    Calculate stress indicators and score based on commit timestamps.

    Args:
    commit_timestamps (list): A list of commit timestamps.

    Returns:
    Optional[dict]: A dictionary with stress indicators, or None if there is not enough data.
    """

    if len(commit_timestamps) < 2:
        return None  # Not enough data for analysis

    commit_times = [datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
                    for ts in commit_timestamps]

    # Sort commit times in ascending order
    commit_times.sort()

    # Calculate time differences between consecutive commits
    time_diffs = [commit_times[i] - commit_times[i-1]
                  for i in range(1, len(commit_times))]

    # Calculate commit frequency (commits per day) over the last window_size commits
    commit_frequency = len(
        commit_times) / (commit_times[-1] - commit_times[0]).total_seconds() / (60 * 60 * 24)

    # Calculate average time gap between commits
    average_time_gap = sum(time_diffs, timedelta()) / len(time_diffs)

    # Determine if there are commits made outside of typical working hours (e.g., after 6 PM)
    after_hours_commits = sum(
        1 for commit_time in commit_times if commit_time.hour >= 18)

    # Calculate high velocity indicator
    high_velocity = any(diff < timedelta(minutes=1) for diff in time_diffs)

    # Calculate stress score based on various indicators
    stress_score = 0

    # Increase stress score if high velocity commits are detected
    if high_velocity:
        stress_score += 6

    # Check for irregular commit times and increase stress score if detected
    if after_hours_commits > 0:
        stress_score += 2

    # Check for high commit frequency and increase stress score if detected
    if commit_frequency > 10:
        stress_score += 2

    return {
        'Commit Frequency': commit_frequency,
        'Average Time Gap': average_time_gap,
        'Stress Indicators': {
            'High Commit Frequency': commit_frequency > 10,
            'Irregular Commit Times': after_hours_commits > 0,
            'High Velocity': high_velocity
        },
        'Stress Score': stress_score
    }
