import subprocess

def dog_output(msg):
    command = f'echo "{msg}"| boxes -d dog -a c'
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, universal_newlines=True)
    # Read the output 
    output, _ = process.communicate()
    return output