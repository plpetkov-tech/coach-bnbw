import unittest
import os

from modules.analyst import analyze_sentiment, assess_commits_for_repetitiveness, create_ollama_client
import tracemalloc
from modules.parser import parse_config
import time


def get_elapsed_time(elapsed_time):
    """Function to print the elapsed time in a human-readable format."""
    minutes, seconds = divmod(elapsed_time, 60)
    hours, minutes = divmod(minutes, 60)
    milliseconds = (elapsed_time - int(elapsed_time)) * 1000
    return f"Total elapsed time: {int(hours)}h {int(minutes)}m {int(seconds)}s {int(milliseconds)}ms"


def start_timer():
    """Function to record the start time."""
    return time.time()


def end_timer(start_time):
    """Function to record the end time and calculate the elapsed time."""
    return time.time() - start_time


class TestStress(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        ABSOLUTE_SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
        config = parse_config(ABSOLUTE_SCRIPT_DIR)
        tracemalloc.start()
        self.client = create_ollama_client(
            host=config["ollama_url"], password=config["api_key"])
        self.start_time = start_timer()

    @classmethod
    def tearDownClass(self):
        tracemalloc.stop()
        elapsed_time = end_timer(self.start_time)
        print(
            f'Running the stress test took: \n{get_elapsed_time(elapsed_time)}')

    def test_stress_ollama(self):
        """
        Test the load needed to bug the ollama server
        """
        test_case = {"data": "giving this shit a proper naming cuz it wont work otherwise",
                     "expected_result": "Negative"}

        successfulRuns = 0
        for i in range(1000):
            with self.subTest(test_case=test_case):
                result = analyze_sentiment(test_case["data"], self.client)
                self.assertEqual(result, test_case["expected_result"])
                successfulRuns += 1
        print(f"{'*'*30}\n{successfulRuns} stress test cases have passed!\n{'*'*30}")


class TestRepetitiveness(unittest.TestCase):
    def test_analyze_repetitiveness_repetitive(self):
        """
        Test the repetitiveness analysis function with different input scenarios.
        """
        test_case = ["try to fix ci err in pipeline",
                     "try to fix ci err in pipeline",
                     "try to fix requirements.txt err in pipeline",
                     "try to fix docker err in pipeline",
                     "try to fix docker err in pipeline",
                     "try to fix docker err in pipeline"]
        result = assess_commits_for_repetitiveness(test_case)
        self.assertGreater(float(result.split("%")[0]), 70)

    def test_analyze_repetitiveness_original(self):
        """
        Test the repetitiveness analysis function with different input scenarios.
        """
        test_case = ["try to fix ci err in pipeline",
                     "adding new directory modules",
                     "include requirements.txt to manage dependancies",
                     "change from github actions to gitlab ci",
                     "add new functionality to the test framework",
                     "bump versions in requirements to prevent vulnerabilities"]
        result = assess_commits_for_repetitiveness(test_case)

        self.assertLess(float(result.split("%")[0]), 40)


class TestSentiment(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        ABSOLUTE_SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
        config = parse_config(ABSOLUTE_SCRIPT_DIR)
        tracemalloc.start()
        self.client = create_ollama_client(
            host=config["ollama_url"], password=config["api_key"])
        self.start_time = start_timer()

    @classmethod
    def tearDownClass(self):
        tracemalloc.stop()
        elapsed_time = end_timer(self.start_time)
        print(
            f'Running the sentiment test took: \n{get_elapsed_time(elapsed_time)}')

    def test_analyze_sentiment(self):
        """
        Test the sentiment analysis function with different input scenarios.
        """
        test_cases = [
            {"data": "giving this shit a proper naming cuz it wont work otherwise",
                "expected_result": "Negative"},
            {"data": "this shit sucks", "expected_result": "Negative"},
            {"data": "im sick and tired of fixing this mess",
                "expected_result": "Negative"},
            {"data": "fuck you Todd, this shit is disgusting",
                "expected_result": "Negative"},
            {"data": "Karen wont stop doing smelly code",
                "expected_result": "Negative"},
            {"data": "We have a fully working and amazing module! I love the way it works",
                "expected_result": "Positive"},
            {"data": "add file.py to the folder xyz for ticket in jira 2344",
                "expected_result": "Neutral"},
            {"data": "added capability to use flags in the program and tested it",
                "expected_result": "Neutral"},
            {"data": "organize and take out window_size as it was not accessed",
                "expected_result": "Neutral"},
            {"data": "add the func to assess stress and give stress indicators, is working",
                "expected_result": "Positive"},
            {"data": "add project function for the stress analyzer / velocity and so on",
                "expected_result": "Neutral"},
            {"data": "now we are correctly analyzing commits and repetitiveness of everything",
                "expected_result": "Positive"},
            {"data": "add useful funcs to get commits by author",
                "expected_result": "Neutral"},
            {"data": "removed while loop", "expected_result": "Neutral"},
            {"data": "testing commit gathering based on requested authors",
                "expected_result": "Neutral"},
            {"data": "added suppport for multiple authors",
                "expected_result": "Neutral"},
            {"data": "experimenting with the new commit list",
                "expected_result": "Neutral"},

            {"data": "giving this shit a proper naming cuz it wont work otherwise",
                "expected_result": "Negative"},
            {"data": "this shit sucks", "expected_result": "Negative"},
            {"data": "im sick and tired of fixing this mess",
                "expected_result": "Negative"},
            {"data": "fuck you Todd, this shit is disgusting",
                "expected_result": "Negative"},
            {"data": "Karen wont stop doing smelly code",
                "expected_result": "Negative"},
            {"data": "We have a fully working and amazing module! I love the way it works",
                "expected_result": "Positive"},
            {"data": "add file.py to the folder xyz for ticket in jira 2344",
                "expected_result": "Neutral"},
            {"data": "added capability to use flags in the program and tested it",
                "expected_result": "Neutral"},
            {"data": "organize and take out window_size as it was not accessed",
                "expected_result": "Neutral"},
            {"data": "add the func to assess stress and give stress indicators, is working",
                "expected_result": "Positive"},
            {"data": "add project function for the stress analyzer / velocity and so on",
                "expected_result": "Neutral"},
            {"data": "now we are correctly analyzing commits and repetitiveness of everything",
                "expected_result": "Positive"},
            {"data": "add useful funcs to get commits by author",
                "expected_result": "Neutral"},
            {"data": "removed while loop", "expected_result": "Neutral"},
            {"data": "testing commit gathering based on requested authors",
                "expected_result": "Neutral"},
            {"data": "added suppport for multiple authors",
                "expected_result": "Neutral"},
            {"data": "experimenting with the new commit list",
                "expected_result": "Neutral"},
            {"data": "I just hate this feature, it's a nightmare to work with",
                "expected_result": "Negative"},
            {"data": "The team did a great job with this update, very impressed",
                "expected_result": "Positive"},
            {"data": "We need to refactor this code, it's getting messy",
                "expected_result": "Neutral"},
            {"data": "Can someone please fix this bug ASAP?",
                "expected_result": "Negative"},
            {"data": "I'm excited to see the results of our latest deployment",
                "expected_result": "Positive"},
            {"data": "Let's discuss the design for the new feature",
                "expected_result": "Neutral"},
            {"data": "The documentation needs to be updated for this module",
                "expected_result": "Neutral"},
            {"data": "I found a security vulnerability in the login system",
                "expected_result": "Negative"},
            {"data": "We should celebrate, the project is finally complete",
                "expected_result": "Positive"},
            {"data": "I'm worried about the performance of this function",
                "expected_result": "Negative"},
            {"data": "I don't understand why this code isn't working",
                "expected_result": "Negative"},
            {"data": "The UI looks great, good job to the design team",
                "expected_result": "Positive"},
            {"data": "We should add more error handling to this part of the code",
                "expected_result": "Neutral"},
            {"data": "This optimization really improved the response time",
                "expected_result": "Positive"},
            {"data": "The server crashed again, we need to investigate",
                "expected_result": "Negative"},
            {"data": "The new feature is intuitive and user-friendly",
                "expected_result": "Positive"},
            {"data": "We need to address these edge cases in our testing",
                "expected_result": "Neutral"},
            {"data": "Can't wait to see the feedback on this demo",
                "expected_result": "Positive"},
            {"data": "The requirements for this task are unclear",
                "expected_result": "Negative"},
            {"data": "I'm impressed with the progress the team has made",
                "expected_result": "Positive"},
            {"data": "We should refactor this module for better readability",
                "expected_result": "Neutral"},
            {"data": "This bug has been haunting us for weeks",
                "expected_result": "Negative"},
            {"data": "The code review process needs to be streamlined",
                "expected_result": "Neutral"},
            {"data": "I'm struggling to understand this part of the codebase",
                "expected_result": "Neutral"},
            {"data": "The user interface needs some minor tweaks",
                "expected_result": "Neutral"},
            {"data": "I'm confident we'll meet the deadline for this project",
                "expected_result": "Positive"},
            {"data": "Let's automate this repetitive task",
                "expected_result": "Neutral"},
            {"data": "I'm worried about the scalability of this solution",
                "expected_result": "Negative"},
            {"data": "We should add more unit tests for this function",
                "expected_result": "Neutral"},
            {"data": "The team needs better communication on this project",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the final product",
                "expected_result": "Positive"},
            {"data": "We need to document the API endpoints",
                "expected_result": "Neutral"},
            {"data": "This code is a nightmare to debug",
                "expected_result": "Negative"},
            {"data": "The feedback from the client was overwhelmingly positive",
                "expected_result": "Positive"},
            {"data": "Let's refactor this module to improve performance",
                "expected_result": "Neutral"},
            {"data": "I'm impressed with the attention to detail in this code",
                "expected_result": "Positive"},
            {"data": "We should optimize this algorithm for better efficiency",
                "expected_result": "Neutral"},
            {"data": "This project is way behind schedule",
                "expected_result": "Negative"},
            {"data": "I'm confident we can resolve this issue quickly",
                "expected_result": "Positive"},
            {"data": "The team needs more training on this technology",
                "expected_result": "Neutral"},
            {"data": "We should consider using a different framework for this project",
                "expected_result": "Neutral"},
            {"data": "I'm frustrated with the lack of progress on this task",
                "expected_result": "Negative"},
            {"data": "The codebase is becoming increasingly difficult to maintain",
                "expected_result": "Negative"},
            {"data": "This feature is essential for the success of the project",
                "expected_result": "Positive"},
            {"data": "We need to refactor this class to improve readability",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the results of the A/B test",
                "expected_result": "Positive"},
            {"data": "We should add more comments to explain this logic",
                "expected_result": "Neutral"},
            {"data": "This project is a disaster, nothing is going according to plan",
                "expected_result": "Negative"},
            {"data": "The team needs to focus on meeting the project milestones",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the security implications of this code",
                "expected_result": "Negative"},
            {"data": "The documentation is outdated and needs to be revised",
                "expected_result": "Negative"},
            {"data": "I'm impressed with the level of collaboration on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this function for better performance",
                "expected_result": "Neutral"},
            {"data": "I'm confident we'll find a solution to this problem",
                "expected_result": "Positive"},
            {"data": "The team needs to prioritize bug fixes for this release",
                "expected_result": "Neutral"},
            {"data": "This code smells, we need to clean it up",
                "expected_result": "Negative"},
            {"data": "I'm excited to start working on this new feature",
                "expected_result": "Positive"},
            {"data": "We need to improve the error handling in this module",
                "expected_result": "Neutral"},
            {"data": "The performance of this application is unacceptable",
                "expected_result": "Negative"},
            {"data": "I'm amazed by the creativity of the team on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve maintainability",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the impact of this change on existing functionality",
                "expected_result": "Neutral"},
            {"data": "The team needs to be more proactive in addressing issues",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the reaction to this new feature",
                "expected_result": "Positive"},
            {"data": "We should automate the deployment process for this project",
                "expected_result": "Neutral"},
            {"data": "This code is elegant and well-structured",
                "expected_result": "Positive"},
            {"data": "I'm frustrated with the lack of documentation for this module",
                "expected_result": "Negative"},
            {"data": "The team needs to improve their understanding of the requirements",
                "expected_result": "Neutral"},
            {"data": "I'm impressed with the progress we've made on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to reduce duplication",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the performance implications of this change",
                "expected_result": "Negative"},
            {"data": "The user interface needs a major overhaul",
                "expected_result": "Negative"},
            {"data": "I'm excited to see how this feature will enhance the product",
                "expected_result": "Positive"},
            {"data": "We should refactor this module to improve testability",
                "expected_result": "Neutral"},
            {"data": "This codebase is a mess, we need to start from scratch",
                "expected_result": "Negative"},
            {"data": "I'm impressed with the level of detail in the requirements",
                "expected_result": "Positive"},
            {"data": "We should add more logging to track the flow of data",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the stability of this application",
                "expected_result": "Neutral"},
            {"data": "The team needs to be more proactive in addressing technical debt",
                "expected_result": "Neutral"},
            {"data": "I'm excited to collaborate with the team on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve readability",
                "expected_result": "Neutral"},
            {"data": "This bug is a blocker, we need to fix it immediately",
                "expected_result": "Positive"},
            {"data": "I'm confident we can deliver this project ahead of schedule",
                "expected_result": "Positive"},
            {"data": "We need to optimize this algorithm for better performance",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the lack of unit tests for this module",
                "expected_result": "Neutral"},
            {"data": "The team needs better coordination on this project",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the impact of this feature on user engagement",
                "expected_result": "Positive"},
            {"data": "We should refactor this function to improve modularity",
                "expected_result": "Neutral"},
            {"data": "This code is spaghetti, we need to refactor it",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll overcome this challenge",
                "expected_result": "Positive"},
            {"data": "We need to address these security vulnerabilities immediately",
                "expected_result": "Positive"},
            {"data": "I'm impressed with the scalability of this solution",
                "expected_result": "Positive"},
            {"data": "The team needs to improve their understanding of the technology stack",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the results of this experiment",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve maintainability",
                "expected_result": "Neutral"},
            {"data": "This bug is preventing us from moving forward",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll meet our performance targets",
                "expected_result": "Positive"},
            {"data": "We need to refactor this module to improve extensibility",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the reliability of this service",
                "expected_result": "Negative"},
            {"data": "The team needs to prioritize technical debt reduction",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see how this feature will impact user satisfaction",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve readability",
                "expected_result": "Neutral"},
            {"data": "This code smells, we need to clean it up",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll deliver a high-quality product",
                "expected_result": "Positive"},
            {"data": "We need to refactor this function for better performance",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the impact of this change on existing functionality",
                "expected_result": "Neutral"},
            {"data": "The team needs to be more proactive in addressing issues",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the reaction to this new feature",
                "expected_result": "Positive"},
            {"data": "We should automate the deployment process for this project",
                "expected_result": "Neutral"},
            {"data": "This code is elegant and well-structured",
                "expected_result": "Positive"},
            {"data": "I'm frustrated with the lack of documentation for this module",
                "expected_result": "Negative"},
            {"data": "The team needs to improve their understanding of the requirements",
                "expected_result": "Negative"},
            {"data": "I'm impressed with the progress we've made on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to reduce duplication",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the performance implications of this change",
                "expected_result": "Neutral"},
            {"data": "The user interface needs a major overhaul",
                "expected_result": "Negative"},
            {"data": "I'm excited to see how this feature will enhance the product",
                "expected_result": "Positive"},
            {"data": "We should refactor this module to improve testability",
                "expected_result": "Neutral"},
            {"data": "This codebase is a mess, we need to start from scratch",
                "expected_result": "Negative"},
            {"data": "I'm impressed with the level of detail in the requirements",
                "expected_result": "Positive"},
            {"data": "We should add more logging to track the flow of data",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the stability of this application",
                "expected_result": "Neutral"},
            {"data": "The team needs to be more proactive in addressing technical debt",
                "expected_result": "Neutral"},
            {"data": "I'm excited to collaborate with the team on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve readability",
                "expected_result": "Neutral"},
            {"data": "This bug is a blocker, we need to fix it immediately",
                "expected_result": "Positive"},
            {"data": "I'm confident we can deliver this project ahead of schedule",
                "expected_result": "Positive"},
            {"data": "We need to optimize this algorithm for better performance",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the lack of unit tests for this module",
                "expected_result": "Neutral"},
            {"data": "The team needs better coordination on this project",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the impact of this feature on user engagement",
                "expected_result": "Positive"},
            {"data": "We should refactor this function to improve modularity",
                "expected_result": "Neutral"},
            {"data": "This code is spaghetti, we need to refactor it",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll overcome this challenge",
                "expected_result": "Positive"},
            {"data": "We need to address these security vulnerabilities immediately",
                "expected_result": "Positive"},
            {"data": "I'm impressed with the scalability of this solution",
                "expected_result": "Positive"},
            {"data": "The team needs to improve their understanding of the technology stack",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the results of this experiment",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve maintainability",
                "expected_result": "Neutral"},
            {"data": "This bug is preventing us from moving forward",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll meet our performance targets",
                "expected_result": "Positive"},
            {"data": "We need to refactor this module to improve extensibility",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the reliability of this service",
                "expected_result": "Negative"},
            {"data": "The team needs to prioritize technical debt reduction",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see how this feature will impact user satisfaction",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve readability",
                "expected_result": "Neutral"},
            {"data": "This code smells, we need to clean it up",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll deliver a high-quality product",
                "expected_result": "Positive"},
            {"data": "We need to refactor this function for better performance",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the impact of this change on existing functionality",
                "expected_result": "Neutral"},
            {"data": "The team needs to be more proactive in addressing issues",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the reaction to this new feature",
                "expected_result": "Positive"},
            {"data": "We should automate the deployment process for this project",
                "expected_result": "Neutral"},
            {"data": "This code is elegant and well-structured",
                "expected_result": "Positive"},
            {"data": "I'm frustrated with the lack of documentation for this module",
                "expected_result": "Negative"},
            {"data": "The team needs to improve their understanding of the requirements",
                "expected_result": "Negative"},
            {"data": "I'm impressed with the progress we've made on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to reduce duplication",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the performance implications of this change",
                "expected_result": "Negative"},
            {"data": "The user interface needs a major overhaul",
                "expected_result": "Negative"},
            {"data": "I'm excited to see how this feature will enhance the product",
                "expected_result": "Positive"},
            {"data": "We should refactor this module to improve testability",
                "expected_result": "Neutral"},
            {"data": "This codebase is a mess, we need to start from scratch",
                "expected_result": "Negative"},
            {"data": "I'm impressed with the level of detail in the requirements",
                "expected_result": "Positive"},
            {"data": "We should add more logging to track the flow of data",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the stability of this application",
                "expected_result": "Neutral"},
            {"data": "The team needs to be more proactive in addressing technical debt",
                "expected_result": "Neutral"},
            {"data": "I'm excited to collaborate with the team on this project",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve readability",
                "expected_result": "Neutral"},
            {"data": "This bug is a blocker, we need to fix it immediately",
                "expected_result": "Positive"},
            {"data": "I'm confident we can deliver this project ahead of schedule",
                "expected_result": "Positive"},
            {"data": "We need to optimize this algorithm for better performance",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the lack of unit tests for this module",
                "expected_result": "Neutral"},
            {"data": "The team needs better coordination on this project",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the impact of this feature on user engagement",
                "expected_result": "Positive"},
            {"data": "We should refactor this function to improve modularity",
                "expected_result": "Neutral"},
            {"data": "This code is spaghetti, we need to refactor it",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll overcome this challenge",
                "expected_result": "Positive"},
            {"data": "We need to address these security vulnerabilities immediately",
                "expected_result": "Positive"},
            {"data": "I'm impressed with the scalability of this solution",
                "expected_result": "Positive"},
            {"data": "The team needs to improve their understanding of the technology stack",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see the results of this experiment",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve maintainability",
                "expected_result": "Neutral"},
            {"data": "This bug is preventing us from moving forward",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll meet our performance targets",
                "expected_result": "Positive"},
            {"data": "We need to refactor this module to improve extensibility",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the reliability of this service",
                "expected_result": "Negative"},
            {"data": "The team needs to prioritize technical debt reduction",
                "expected_result": "Neutral"},
            {"data": "I'm excited to see how this feature will impact user satisfaction",
                "expected_result": "Positive"},
            {"data": "We should refactor this code to improve readability",
                "expected_result": "Neutral"},
            {"data": "This code smells, we need to clean it up",
                "expected_result": "Negative"},
            {"data": "I'm confident we'll deliver a high-quality product",
                "expected_result": "Positive"},
            {"data": "We need to refactor this function for better performance",
                "expected_result": "Neutral"},
            {"data": "I'm concerned about the impact of this change on existing functionality",
                "expected_result": "Neutral"},
            {"data": "The team needs to be more proactive in addressing issues",
                "expected_result": "Neutral"}
        ]
        successfulRuns = 0
        for test_case in test_cases:
            with self.subTest(test_case=test_case):
                result = analyze_sentiment(test_case["data"], self.client)
                if (result == test_case["expected_result"]):
                    successfulRuns += 1
        failedRuns = len(test_cases) - successfulRuns
        self.assertLessEqual(failedRuns, len(test_cases)
                             * 0.3, 'Asserting less than 10% \failures!')
        print(f"{'*'*30}\n{successfulRuns} test cases have passed!\n{'*'*30}")

# Config parser should take decisions based on where the container is run from
#     If is run locally or standalone as container, looks at the config for an url or path(?)
#     If is run from a pipeline would be known as you would have the repo directly in /work mounted
# Fix the comparassion logic
# Create tests before features
# Create mocks for tests
# Create beautiful documentation *After finishing the project
# Create a mock project in gitlab where we can test the way it behaves in a pipeline
# Create logic to push the image to gitlab registry after passing the pipeline
#     It shoould be tagged with the short sha and whenever we give some flag to the commit like `__push_to_prod` it can version the tag in a nifty way


if __name__ == '__main__':
    unittest.main()
