- [ ] Prepare all texts for the submission     .   
    - [ ] Very important the pitch to be catchy and convincing     .   

# DEMO VIDEO PART
- [ ] Shoot the video     .   
    - [ ] Stressed part (AI assisted)     .   
        - [ ] Screen captures of above reaction in Coach BNBW output     .   
    - [ ] Going to take a break part (AI assisted)     .   
        - [ ] Screen captures of above reaction in Coach BNBW output     .   
    - [ ] Relaxed part (AI assisted)     .   
        - [ ] Screen captures of above reaction in Coach BNBW output     .   
    - [ ] Malefic part (AI assisted)     .   
        - [ ] Screen captures of above reaction in Coach BNBW output     .   
    - [ ] Happy part (AI assisted)     .   
        - [ ] Screen captures of above reaction in Coach BNBW output     .   
- [ ]  Some powerpoint presentation or at least slides to have in transitions in the video   .   
- [ ]  The voiceover of the whole thing    .   

- [ ]  Make the final release docker image and push it to dockerhub    .
- [ ]  Write some other tests just to have more code coverage   .
- [ ]  Add some linting to the pipeline and project   .   
- [ ]  Fix and create something useful with the thresholds   . 
- [ ]  Make the configuration that is available through the configuration file accessible through flags in the container   .
- [ ]  Fix the documentation or create a minimal wiki .md document in the repo instead of all this BS we tryna do with the autogeneration of documentation   .   

jupyter notebook \
     --NotebookApp.allow_origin='https://colab.research.google.com' \
     --port=8888 \
     --NotebookApp.port_retries=0 --allow-root
