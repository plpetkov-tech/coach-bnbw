# Use an official Python runtime as a parent image
FROM python:3.10-slim

# Set the working directory to /app
WORKDIR /app
RUN apt update
RUN apt install git boxes -y
# Copy the current directory contents into the container at /app
COPY main.py /app/
COPY modules /app/modules
COPY requirements.txt /app/requirements.txt

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt

# Create /work directory
RUN mkdir -p /work
# Set the entry point to your script
ENTRYPOINT ["python", "main.py"]

# Specify the default command to run on container start
CMD []
