import os
import argparse
from modules.parser import *
from modules.output import dog_output
from modules.decision_manager import decisions_workflow

def main() -> None:

    """
    Utilizes the modules and serves as the main function of the program.     

    Parses command-line arguments, loads the configuration, makes decisions based on the configuration,
    and prints the generated dog training advice.

    Command-Line Arguments:
        --config        Specify the path to the configuration file. If not provided, defaults to '/app/config.default.toml'.

    Example:
        python script_name.py --config /path/to/custom/config.toml

    Note:
        - The default configuration path is '/app/config.default.toml'.
    """

    # Parser configuration
    parser = argparse.ArgumentParser(description='Coach BNBW, Be nice, Be well!')
    parser.add_argument('--config', type=str, help='Path to the configuration file')

    args = parser.parse_args()

    if not args.config:
        print('Config not specified, trying to use defaults')
        config_path = '/app/config.default.toml'
        os.environ['default_config_coach'] = "1"
    else: 
        config_path = args.config
    
    conf_obj = parse_config(config_path)
    advices = decisions_workflow(conf_obj)
    print(dog_output(advices))
    
if __name__ == "__main__":
    main()
    exit(0)